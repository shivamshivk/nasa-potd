package com.nebyso.nasapotd.api

object ApiConfig {

  //	private var BASE_URL = "192.168.43.113:3003"
  private var BASE_URL = "https://api.nasa.gov/"

  fun getBaseUrl(): String {
    return BASE_URL
  }

}
