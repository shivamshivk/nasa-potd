package com.nebyso.nasapotd.api


import com.google.gson.GsonBuilder
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object ApiClient {

    private lateinit var retrofit: Retrofit
    private val okHttpClient = OkHttpClient()

    val client: Retrofit
        get() {
            val gson = GsonBuilder().setLenient().create()
            retrofit = Retrofit.Builder()
                .baseUrl(ApiConfig.getBaseUrl())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .client(
                    okHttpClient.newBuilder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(
                        10,
                        TimeUnit.SECONDS
                    ).writeTimeout(10, TimeUnit.SECONDS).build()
                )
                .build()
            return retrofit
        }

}
