package com.nebyso.nasapotd.api

import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.QueryMap


interface ApiInterface {

    @GET("planetary/apod")
    fun getPictureOfTheDay( @QueryMap queryMap: HashMap<String, String>): Call<ResponseBody>

}
