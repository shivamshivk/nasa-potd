@file:Suppress("NULLABILITY_MISMATCH_BASED_ON_JAVA_ANNOTATIONS")

package com.nebyso.nasapotd.utils

import java.text.SimpleDateFormat
import java.util.*

object DateUtils {

    fun formatDate(date: String?, dateFormat: String, toFormat: String): String {
        val df = SimpleDateFormat(dateFormat, Locale.ENGLISH)
        val tf = SimpleDateFormat(toFormat, Locale.ENGLISH)
        df.timeZone = TimeZone.getTimeZone("UTC")
        val newDate = df.parse(date)
        return tf.format(newDate)
    }


}
