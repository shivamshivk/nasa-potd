package com.nebyso.nasapotd.ui

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.content.Intent
import android.graphics.Color
import android.net.Uri
import android.os.Bundle
import android.text.method.ScrollingMovementMethod
import android.util.Log
import android.view.View
import android.webkit.WebChromeClient
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.Player
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.source.ProgressiveMediaSource
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory
import com.google.android.exoplayer2.util.Util
import com.nebyso.nasapotd.R
import com.nebyso.nasapotd.api.ApiClient
import com.nebyso.nasapotd.api.ApiInterface
import com.nebyso.nasapotd.utils.DateUtils
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar_home.*
import okhttp3.ResponseBody
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Response
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap


class MainActivity : AppCompatActivity(),View.OnClickListener {

    private lateinit var calendar: Calendar
    private lateinit var date: DatePickerDialog.OnDateSetListener
    private var dateS:String=""
    private var mediaType:String="image"
    private var url:String=""
    private lateinit var player: SimpleExoPlayer
    private lateinit var mediaDataSourceFactory: DefaultDataSourceFactory


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()
        setListeners()
    }

    private fun initViews() {

        descTv.movementMethod= ScrollingMovementMethod()

        calendar = Calendar.getInstance()
        date = DatePickerDialog.OnDateSetListener { _, year, monthOfYear, dayOfMonth ->
            calendar.set(Calendar.YEAR, year)
            calendar.set(Calendar.MONTH, monthOfYear)
            calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
            updateLabel()

            getPictureOfTheDay()
        }
        updateLabel()

        getPictureOfTheDay()
    }

    private fun updateLabel() {
        val myFormat = "yyyy-MM-dd" //In which you need put here
        val sdf = SimpleDateFormat(myFormat, Locale.getDefault())
        dateS = sdf.format(calendar.time).toString()

        travelDateTv.text = DateUtils.formatDate(dateS,"yyyy-MM-dd","dd/MM/yyyy")
    }


    private fun setListeners() {
        playzoomBtn.setOnClickListener(this)
        calendarIv.setOnClickListener(this)
        nasaIv.setOnClickListener(this)
    }


    override fun onClick(v: View?) {
        when(v?.id){
            R.id.playzoomBtn->{
                if(mediaType=="image"){
                    val intent = Intent(applicationContext,FullScreenImageActivity::class.java)
                    intent.putExtra("imgUrl",url)
                    startActivity(intent)
                }
            }
            R.id.calendarIv->{
                val dpd = DatePickerDialog(
                    this, date, calendar
                        .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)
                )
                dpd.datePicker.maxDate = Date().time
                dpd.show()
            }
        }
    }


    private fun getPictureOfTheDay(){



        placeHolderPBar.visibility = View.VISIBLE
        viewLayout.visibility = View.GONE
        pageNotFoundLayout.visibility = View.GONE

        val apiService = ApiClient.client.create(ApiInterface::class.java)

        val queryParams = HashMap<String, String>()
        queryParams["api_key"] = "DEMO_KEY"
        queryParams["date"] = dateS

        val deleteAd = apiService.getPictureOfTheDay(queryParams)
        deleteAd.enqueue(object : retrofit2.Callback<ResponseBody> {
            @SuppressLint("SetTextI18n")
            override fun onResponse(call: Call<ResponseBody>, response: Response<ResponseBody>) {

                placeHolderPBar.visibility = View.GONE

                if (response.isSuccessful) {
                    val json = JSONObject(response.body()!!.string())
                    mediaType = json.getString("media_type")

                    if(mediaType=="image"){
                        playzoomBtn.setImageResource(R.drawable.zoom)
                        url = json.getString("hdurl")
                        showImageUI(url)
                    }else{
                        playzoomBtn.setImageResource(R.drawable.play_button)
                        url =json.getString("url")
                        if(url.contains("youtube")){
                            showYouTubeViewUI(json.getString("url"))
                        }else{
                            showVideoUI(json.getString("url"))
                        }
                    }

                    //Displaying the title
                    titleTv.text = json.getString("title")

                    //Displaying the description
                    descTv.text = json.getString("explanation")


                } else {
                    showErrorMessageUI("Page Not Found !!")
                }

            }

            override fun onFailure(call: Call<ResponseBody>, t: Throwable) {
                placeHolderPBar.visibility = View.GONE
                showErrorMessageUI("Oops! Some Error Occurred")
                Log.e("LoginFrag", "Failed : " + t.message.toString())
            }
        })

    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun showYouTubeViewUI(videoUrl: String) {
        nasaIv.visibility=View.GONE
        nasaEpv.visibility = View.GONE
        webview_player_view.visibility = View.VISIBLE
        webview_player_view.webChromeClient = MyWebChromeClient()
        webview_player_view.webViewClient = webClient()
        webview_player_view.settings.setSupportZoom(true)
        webview_player_view.settings.javaScriptEnabled = true
        webview_player_view.loadUrl(videoUrl)

        viewLayout.visibility = View.VISIBLE
    }


    private fun showVideoUI(videoUrl:String) {
        nasaIv.visibility=View.GONE
        nasaEpv.visibility = View.VISIBLE
        webview_player_view.visibility = View.GONE
        initExoPlayer(videoUrl)
        viewLayout.visibility = View.VISIBLE
    }

    @SuppressLint("CheckResult")
    private fun showImageUI(imageUrl:String) {
        nasaIv.visibility=View.VISIBLE
        nasaEpv.visibility = View.GONE
        webview_player_view.visibility = View.GONE

        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.rectangle1)
        requestOptions.error(R.drawable.rectangle1)

        Glide.with(applicationContext).asBitmap()
            .apply(requestOptions)
            .load(imageUrl)
            .into(nasaIv)

        viewLayout.visibility = View.VISIBLE
    }


    @SuppressLint("SetTextI18n")
    private fun initExoPlayer(videoUri:String){
        player = ExoPlayerFactory.newSimpleInstance(this)

        mediaDataSourceFactory = DefaultDataSourceFactory(this, Util.getUserAgent(this, "mediaPlayerSample"))

        val mediaSource = ProgressiveMediaSource.Factory(mediaDataSourceFactory)
            .createMediaSource(Uri.parse("https://nebyso-uploads.s3.ap-south-1.amazonaws.com/userVideos/5ec8d34c8d6e3608777daa8d2020-06-19T16%3A25%3A59.555Z"))


        with(player) {
            prepare(mediaSource, false, false)
            playWhenReady = true
        }

        player?.addListener(object : Player.DefaultEventListener() {
            override fun onPlayerStateChanged(playWhenReady: Boolean,playbackState: Int) {
                when (playbackState) {
                    Player.STATE_IDLE -> {
                    }
                    Player.STATE_BUFFERING -> {
                        videoProgressBar.visibility = View.VISIBLE
                    }
                    Player.STATE_READY -> {
                        videoProgressBar.visibility = View.GONE
                    }
                    Player.STATE_ENDED -> {

                    }
                }
            }
        })

        nasaEpv.setShutterBackgroundColor(Color.TRANSPARENT)
        nasaEpv.player = player
        nasaEpv.requestFocus()

    }





    private fun showErrorMessageUI(message:String){
        pageNotFoundLayout.visibility = View.VISIBLE
        viewLayout.visibility = View.GONE
        errorTv.text = message
    }

    override fun onDestroy() {
        super.onDestroy()

    }

    private fun releasePlayer() {
        player.release()
    }



    private fun pausePlayer() {
        player.playWhenReady = false
        player.playbackState
    }

    private fun startPlayer() {
        player.playWhenReady = true
        player.playbackState
    }

//    public override fun onStart() {
//        super.onStart()
//
//        if (Util.SDK_INT > 23) initExoPlayer()
//    }

    public override fun onResume() {
        super.onResume()

        if (Util.SDK_INT <= 23) startPlayer()
    }

    public override fun onPause() {
        super.onPause()

        if (Util.SDK_INT <= 23) pausePlayer()
    }

    public override fun onStop() {
        super.onStop()

        if (Util.SDK_INT > 23) pausePlayer()
    }


    inner class MyWebChromeClient : WebChromeClient() {
        override fun onProgressChanged(view: WebView, newProgress: Int) {
            videoProgressBar.visibility = View.VISIBLE
        }
    }

    inner class webClient : WebViewClient() {
        override fun shouldOverrideUrlLoading(view: WebView, url: String): Boolean {
            view.loadUrl(url)
            return true
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageFinished(view, url)
            videoProgressBar.visibility = View.GONE
        }
    }

}
