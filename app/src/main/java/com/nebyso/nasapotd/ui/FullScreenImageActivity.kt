package com.nebyso.nasapotd.ui

import android.annotation.SuppressLint
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.nebyso.nasapotd.R
import kotlinx.android.synthetic.main.activity_full_screen_image.*

class FullScreenImageActivity : AppCompatActivity() {

    @SuppressLint("CheckResult")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_full_screen_image)

        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.rectangle1)
        requestOptions.error(R.drawable.rectangle1)

        Glide.with(applicationContext).asBitmap()
            .apply(requestOptions)
            .load(intent.getStringExtra("imgUrl"))
            .into(fullScreenIv)

        backBtn.setOnClickListener{
            finish()
        }
    }
}
